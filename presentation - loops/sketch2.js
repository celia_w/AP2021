function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(0);
  strokeWeight(4);
  stroke(255);

/*a "for loop" condenses the variables and conditions into one line within ()
a "nested loop" makes the two loops go together - for every x, do a line of y*/
  for (var x = 0; x <= width; x = x + 100) {
    for (var y = 0; y <= width; y = y + 100) {
      fill(255);
      ellipse(x, y, 50, 50);
    }
  }
}
