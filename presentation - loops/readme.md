**While () and for () loops**
[Code for while loop and for loop](https://gitlab.com/celia_w/AP2021/-/blob/master/presentation%20-%20loops/sketch.js)

![](https://gitlab.com/celia_w/AP2021/-/raw/master/presentation%20-%20loops/Screenshot_2021-02-15_at_16.55.39.png)


**Nested loops**
[Code for nested loop](https://gitlab.com/celia_w/AP2021/-/blob/master/presentation%20-%20loops/sketch2.js)

![](https://gitlab.com/celia_w/AP2021/-/raw/master/presentation%20-%20loops/Screenshot_2021-02-15_at_16.55.45.png)

**What happens when you don't make a nested loop, for two seperate loops?**
[Example code](https://gitlab.com/celia_w/AP2021/-/blob/master/presentation%20-%20loops/sketch3.js)

![](https://gitlab.com/celia_w/AP2021/-/raw/master/presentation%20-%20loops/Screenshot_2021-02-15_at_16.55.53.png)

**Slideshow on time**

https://docs.google.com/presentation/d/e/2PACX-1vRsZ-Eqs2t1-5esgs36bbdG9C6FsXp9CN79ad4NHkfOgvXsN5a4yEZUBY99q1tDmy7fZ-7tsKOdbrq_/pub?start=false&loop=false&delayms=3000

