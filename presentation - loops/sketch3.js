function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(0);
  strokeWeight(4);
  stroke(255);

//a "for loop" condenses the variables and conditions into one line within ()
//a "nested loop" makes the two loops go together - for every x, do a line of y
  for (var x = 0; x <= width; x = x + 100) {
      fill(255);
      ellipse(x, 200, 50, 50);
  }

//writing each loop on it's own, creates 2 seperate lines instead
  for (var y = 0; y <= width; y = y + 100) {
    fill(255);
    ellipse(200, y, 50, 50);
  }
}
