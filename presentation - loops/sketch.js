function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(0);
  strokeWeight(4);
  stroke(255);

/*
1. keyword
2. initialization
3. condition
4. itirization statement
*/

//the "while loop" is similar to the "if ()" function. main difference is,
//that the "while loop" does it over and over and the "if ()" function does it only once
  var x = 0;
  while (x <= width){
    fill(255);
    ellipse(x, 100, 25, 25);
    x = x + 50; //arythmatic operator - increment, decrement, other
  }

//a "for loop" condenses the variables and conditions into one line within ()
  for (var x = 0; x <= width; x = x + 100) {
      fill(255);
      ellipse(x, 200, 50, 50);
  }
}
