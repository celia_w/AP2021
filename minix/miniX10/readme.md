**–  What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Generally speaking we did not have much trouble making the flowchart for our ideas, but we spent more time on trying to figure out and formulate our ideas, in order to have a specified starting point. Once we had most of it figured out, making the flowcharts went pretty fast. 

The difficult part was to make something that would make sense for each member in the group in terms of the conceptualization of our ideas. We have included the essential overall features in our charts, but there are of course a lot of variables that have not been accounted for, that each member has a vague idea about. These are mostly the aesthetic values of our project ideas, whose symbolic meanings have yet to be determined once we choose an idea and dig deeper into its conceptual significance.

It should also be mentioned that we made our flowcharts on a Miro board, which enabled us all to make and edit them at the same time. This made it a lot easier in terms of communication and worked as a very good solution to making all of this online.

We observed that when the flowchart was made in the group prior to the code for our next project, the focus centered on defining the logical processes more than how the code would “think” and work. Thereby we had more focus on simple communication of the idea. This was opposed to some of our individual flowcharts, which due to the fact that we had a code prewritten, more easily had a focus on the algorithmic complexity, and the use of language in the flow charts also resembled how language was used on a code level. 


**–  What are the technical challenges facing the two ideas and how are you going to address these?**
 							
We tried to bridge the gap in our skill levels by discussing each idea thoroughly, not just conceptually but also technically. Both ideas that were chosen have a low fidelity solution connected to it, ensuring that a final product can be achieved without members getting left out of the programming process. This is very important, as we want everyone to be able to talk about and explain the technical aspects of the code that we are going to make, as we all have our strengths and weaknesses in this course.

Personally, it was also a challenge to figure it out with so many people, as it could get a bit chaotic at times. People have different ideas for how to do it, and then we had to figure out how to do it together - and for it to make sense!
								
**–  In which ways are the individual and the group flowcharts you produced useful?**

Personally, the individual flowchart could be hard to make, since it was up to myself how I wanted to do it. Additionally, I didn't have others to worry about, that would have to understand it - as was the case for our group flowchart. The most challenging part was deciding how to write it, how technical or simple to make it. It was very different from the group on, since it was done _after_ making the code. It was a bit easier to look at the code and then decide how to do it, rather than doing it before you know how it should look. 
				
The group flowcharts act as blueprints for the final sketch. Through dialogue and knowledge from the course all other details can be abstracted by each respective member.  		

![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX10/Flowchart_from_miniX9.png)

![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX10/flowchart_dataficering.png)

![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX10/flowchart_generative_art.png)


**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
<br>
Taina Bucher, “The Multiplicity of Algorithims,” If...Then: Algorithmic Power andPolitics (Oxford: Oxford University Press, 2018)
<br>
Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013
