class Cloud1 {
  constructor() {
  this.speed = floor(random(1, 2));
  this.size = floor(random(100,300));
  this.size2 = floor(random(15, 25));
  this.pos = new createVector(width+300, random(12, height/1.2));
  }

  move() {  //moving behaviors
    this.pos.x-=this.speed;
  }

  show() {
    push();
    noStroke();
    fill(250, 90);
    ellipse(this.pos.x, this.pos.y, this.size, this.size2);
    pop();
  }
}

class Cloud2 {
  constructor() {
  this.speed = floor(random(1, 2));
  this.size = floor(random(100,300));
  this.size2 = floor(random(15, 25));
  this.pos = new createVector(-300, random(12, height/1.2));
  }

  move() {  //moving behaviors
    this.pos.x+=this.speed;
  }

  show() {
    push();
    noStroke();
    fill(250, 90);
    ellipse(this.pos.x, this.pos.y, this.size, this.size2);
    pop();
  }
}
