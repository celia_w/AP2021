let data;

function preload() {
  data = loadJSON("words.json");
}

function setup() {
  noCanvas();

  let words = data.words

  for (var i = 0; i < words.length; i++) {
    createElement("h1", words[i].Term)
    let neutral = words[i].Neutral;
    for (var j = 0; j < neutral.length; j++) {
      createDiv(neutral[j]);
    }
  }
}
