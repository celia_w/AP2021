**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX8/Screenshot_2021-04-11_at_13.56.19.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX8/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX8/sketch.js)



**“The gender-neutral dictionary”**

When you open our project you see a bunch of work-terms that are gender-bound, but underneath each one you’ll see a gender-neutral version of that word. The neutral words aren’t vertical because we wanted the viewer to feel like it wasn’t necessarily easy to read them. We did this in order to make the viewer reflect upon how it isn’t easy using gender-neutral words when you’ve previously been conditioned to using the gender-bound names. We have to put in work in order for us to start using more gender-neutral words in the future, if we want our language to reflect an ideal society where all identities are equal and accepted.

**Describe how your program works, and what syntax you have used, and learnt?**

For our program we used a nested for-loop, and a preLoad function as well, to load our JSON file. Our program is extremely simple and was inspired by TheCodingTrain’s own code when demonstrating JSON files. We spent more time on the JSON, to put in the info and set it up. In general, the program isn’t super difficult to read through nor decode. We found this weeks miniX to be very challenging in terms of the conceptual and visual department, so we focused more on understanding the JSON file and how to insert it. 


**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).**

As has been mentioned before, programming is introducing a need for a new definition of literacy. Programming is complex, yet opens up a lot of new possibilities in our increasingly tech-fixated world. When making a program, like ours, it’s equally as important to note the visual concept you see, as to delve into the code and understand that one as well. As mentioned in the book Vocable Code: 

> neither the score nor the program script can be detached from its performance, (Cox and McLean, p. 22)

meaning that the code itself and the performance goes together. This is especially important when trying to make something that has a political/social/cultural message which conveyment depends on its visual aspects as well as the execution of the code.

In our code, it seems that things are rather straightforward, in that the nested for-loop simply just says what the JSON file reads. However, once you go to the visual program, it suddenly becomes harder to understand and read. Where the JSON file is neatly set up, and it’s easy to find each category and its terms, the program is harder to figure out and not as easy to read - it poses a struggle for the reader to understand what’s going on, as well as encouraging the reader or viewer to spend some time in figuring it out.


**How would you reflect on your work in terms of Vocable Code?**

Looking at Cox and McLeans text Vocable code we saw how they mentioned that 

> ... meaning is organized out of differences between elements that are meaningless in themselves, (Cox and McLean, p. 19)

 which we felt like could fit with our project in a way. The terms don’t have any other meaning to them other than the actual meaning of the word and the same goes for the neutral words, but by setting them up next to each other like we did then a different meaning gets created. In that way the differences between these two types of terms/words create a meaning. 


**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186
<br>
Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38
<br>
Allison Parrish, “Text and Type” (2019), https://creative-coding.decontextualize.com/text-and-type/
