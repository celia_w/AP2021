function setup() {
  // put setup code here
  createCanvas(700,700); //canvas size
  background(103,154,84); //background
  console.log("pika? piiikaaaachuuuuu!");
}

function draw() {
  // put drawing code here

  textSize(30); //text
  textAlign(CENTER);
  fill("black");
  text("press any key", 350, 670);

//pikachu's face
  fill(244,220,38);
  stroke(244,220,38);
  strokeWeight(1);
  ellipse(350,425, 400,400);
  ellipse(475,200,100,300); //right ear
  ellipse(225,200,100,300); //left ear

//black ear tips
  fill(0,0,0); //ear marking colour
  strokeWeight(0);
  translate(width / 1, height / 1);
  rotate(PI / 1.0);
  arc(475,525,107,300,-51, PI+QUARTER_PI, PIE); //left ear
  arc(225,525,107,300,-51, PI+QUARTER_PI, PIE); //right ear

//cheeks
  stroke(233,41,41);
  strokeWeight(100);
  point(475, 195); //left
  point(225, 195); //right

//black eyes
  stroke(0,0,0);
  strokeWeight(70);
  point(450, 350); //left
  point(250, 350); //right

//white in eye here
  stroke("white");
  strokeWeight(15);
  point(430, 360); //left
  point(230, 360); //right

//nose
  fill(0,0,0);
  strokeWeight(0);
  ellipse(350, 250, 60, 20);

//mouth + if keyispressed
  if (keyIsPressed === true) {
    fill(266,113,104);
  } else {
    fill(244,220,38);
  }
  ellipse(350,150, 90,110);
}
