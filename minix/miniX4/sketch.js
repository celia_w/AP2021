let capture;
let ctracker;
let img;
let contact = false;

function setup() {
  createCanvas(640, 480);
  background(0);

  //input text
  input = createInput();
  input.size(250, 10);
  input.position(100, 250);

  //submit button
  button = createButton('submit');
  button.position(input.x + input.width, 250);
  button.mousePressed(submit);

  //text
  fill(255);
  textSize(25)
  text("enter name here", 100, 225);

  //video
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}

function draw() {
  if (contact == true) {
      image(capture, 0, 0, 640, 480);

  let positions = ctracker.getCurrentPosition();

  //point 60 is mouth
  for (let i=0; i<positions.length; i++){
    noStroke();
    fill(255, 0, 0);
    ellipse(positions[i][0], positions[i][1], 4, 4);

    fill(0);
    text("thank you for exposing yourself", 10, 50);
    input.hide();
    button.hide();
  }
  }
}

function submit() {
    contact = true;
}
