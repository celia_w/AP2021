**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX4/Screenshot_2021-03-05_at_11.58.52.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX4/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX4/sketch.js)

         

The title of this artwork is **"You Agreed"**, and is meant to criticise how the internet asks you to submit and agree to a lot of things, while knowing you won't always be reading all the terms and conditions. In my artwork, once submitting your name to the site, you're welcomed by seeing your own face along with the message "thank you for exposing yourself". Your face is tracked by red dots, which gives the impression of the software or program actively tracking your face at that exact moment. The program is meant to bring focus on these terms and conditions, and make people more aware of what they submit themselves to, when submitting their information.


My program makes use of the clmtracker made available here: https://github.com/auduno/clmtrackr. Additionally, the use of an input and a button was important, in order to set up the artwork. A for loop was used to continously track the red dots that make up the face model, so as to give the impression of being watched. I made the input box and submit button disappear after submitting, so the text and surprise of seeing your own face would speak for itself.


The code was tricky to work out, and I scrapped a few concepts before ending up with this one. This one gives a stronger impression on the user though, but I definitely plan on expanding my horizon for the next assignment. I felt a little lost with this one and unable to figure out a fun concept.


My program, as I've also described a little bit in the introduction, criticises data capture and the whole idea of accepting or submitting information. Truly, you can never _really_ be sure your data is safe, can you? Culturally, data capture is weird to relate to. You're always a little wary of entering your information on websites, yet still need to do so in order to participate. There's a fine line between what is acceptable and what is not - and sometimes you'll find yourself unable to really do anything.
<br>


**References**: 

https://editor.p5js.org/kjhollen/sketches/S1bVzeF8Z
<br>
https://p5js.org/reference/#/p5/createImg
<br>
https://editor.p5js.org/p5/sketches/Dom:_Input_Button


**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
<br>
Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary”
<br>
Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021
