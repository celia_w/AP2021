let x = 0;
let y = 0;
let xpos;
let ypos;
let spacing = 20;
let size;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(5);
  xpos = random(width);
  ypos = random(height);
  size = random(1, 3);

}

function draw() {
for (var x = 0; x <= width; x = x + 20) {
  stars();
}

}

function stars() {
  ellipse(random(width), random(height), random(1, 4));
  if (random(1) > 0.3 &&  random(1) < 0.5) {
    stroke(255);
  } else if (random(1) > 0.5){
    stroke(249,246,208);
  } else {
    stroke(244,237,162);
  }
}
