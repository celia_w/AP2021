**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX6/Screenshot_2021-03-19_at_13.50.04.png)

**Program link:**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX6/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX6/sketch.js)


In this miniX I decided to redo my last one (miniX5). In  that one, I hadn't fully completed the objective, as I was missing a for-loop. The most important changes from the previous to the current version, was the addition of a for-loop, and the removal of the "clear screen" via mouse Pressed syntax. In this one, I wanted to follow the initial objective, though it didn't turn out like the original and therefore further from what I originally envisioned. 

Demonstating aesthetic programming in my work is hard, as I'm someone who tries to stay neutral and a little out of sight of big cultural problems and the like. I would like to help bring attention to certain subjects, or ask critical questions with my work, but in all honesty I find it hard to get too involved because of how heated debates can get. 

In this work, my critique is more personal than widespread, as I had trouble connecting the definition of "generative" with the for-loop. As it turns out, the for-loop worked better than I had imagined, but one big flaw was how fast it went. The calm and slow presence of the stars in the first iteration was more to my liking, while this one felt much too fast. My work doesn't really demonstrate the perspective of critical-aesthetics much - its message is very neutral and the second iteration seems to change the initial message of "sit back and relax". While I find the class extremely interesting, it can be hard to look at things in a critical way sometimes, as you often get an idea for something simple - such as stars rather than it bringing attention to a bigger subject.

In general, programming can be used to critique and raise questions about a bunch of subjects, many of which we have seen in class already - it's just about doing it in the right way or getting the right idea. Programming as a method for design is interesting, because you can relatively simply get lots of quick ideas out visually, that give another perspective than a hands-on object could. Programming, and how one does it, also relates a lot to the digital culture - imagery of hacking or graphic design comes to mind and while programming is neither of those, it doens't feel too far off at times. 

**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
<br>
Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28
