function setup() {
  createCanvas(500, 500);
}

function draw() {
  background(100);
  frameRate(5);

  let words1 = ['U', 'u', 'o', 'O', 'Ò', 'Ù', 'ò', 'ù'];
  let words2 = ['U', 'u', 'o', 'O', 'Ó', 'Ú', 'ó', 'ú'];
  let words3 = ['w', 'W', 'v', 'V', '_', '-', '*'];
  let eye1 = random(words1);
  let eye2 = random(words2);
  let mouth = random(words3);

  textSize(50);
  textAlign(CENTER);
  fill(random(255),random(255),random(255));
  text(eye1, 210 , 250);
  fill(random(255),random(255),random(255));
  text(eye2, 290 , 250);
  fill(random(255),random(255),random(255));
  text(mouth, 250, 275);

if (mouseIsPressed) {
  fill("black");
  rectMode(CENTER);
  rect(250, 250, 175, 40, 20);
  fill("white");
  textSize(20);
  text("don't change", 250, 255);
} else {
  
}

}
